const mongoose = require("mongoose");
const usuario = mongoose.Schema({
  nombre: {
    type: String,
    
  },
  apellido: {
    type: String,
    
  },
  edad: {
    type: Number,
    
  },
  curp: {
    type: String,
    
  },
});
module.exports = mongoose.model("usuario", usuario);
