const express = require("express");

const areasSchema = require("../model/areaslimpieza");
const router = express.Router();

router.post("/areass", async (req, res) => {
  // res.send("areas creado")
  const areas = await areasSchema(req.body);
  areas
    .save()
    .then((data) => {
      res.json(data);
    })
    .catch((err) => {
      res.json({ mensaje: err });
    });
});

router.get("/areass", async (req, res) => {
  try {
    const areas = await areasSchema.find().exec();
    res.json(areas);

    console.log(areas);
  } catch (error) {
    console.log(error);
  }
});

router.put("/areass/:id", async (req, res) => {
    const {id} = req.params;
    console.log(id);

  
  const areas = await areasSchema(req.body);
  console.log(areas);
  areasSchema.findByIdAndUpdate(
    id, {$set: {
        area : areas.area,
        formalimpiar : areas.formalimpiar
        }}
    , function (err, docs) {
    if (err){
        console.log(err)
        res.json({ mensaje: err });
    }
    else{
        console.log("actualizado : ", docs);
        res.json({ mensaje: "actualizado" });
    }

});
});

router.delete("/areass/:id", async (req, res) => {
  try {
    const {id} = req.params;  
    const eliminado = await areasSchema.findByIdAndDelete(id);
    res.json({ mensaje: "Eliminado" });
  } catch (error) {
    console.log(error);
  }
  
});

module.exports = router;
