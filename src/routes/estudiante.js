const express = require("express");

const estudianteSchema = require("../model/estudiante");
const router = express.Router();

router.post("/estudiantes", async (req, res) => {
  // res.send("estudiante creado")
  const estudiante = await estudianteSchema(req.body);
  estudiante
    .save()
    .then((data) => {
      res.json(data);
    })
    .catch((err) => {
      res.json({ mensaje: err });
    });
});

router.get("/estudiantes", async (req, res) => {
  try {
    const estudiante = await estudianteSchema.find().exec();
    res.json(estudiante);

    console.log(estudiante);
  } catch (error) {
    console.log(error);
  }
});

router.put("/estudiantes/:id", async (req, res) => {
    const {id} = req.params;
    console.log(id);

  
  const estudiante = await estudianteSchema(req.body);
  console.log(estudiante);
  estudianteSchema.findByIdAndUpdate(
    id, {$set: {
        matricula : estudiante.matricula,
        idusuario : estudiante.idusuario,
        
        }}
    , function (err, docs) {
    if (err){
        console.log(err)
        res.json({ mensaje: err });
    }
    else{
        console.log("actualizado : ", docs);
        res.json({ mensaje: "actualizado" });
    }

});
});

router.delete("/estudiantes/:id", async (req, res) => {
  try {
    const {id} = req.params;  
    const eliminado = await estudianteSchema.findByIdAndDelete(id);
    res.json({ mensaje: "Eliminado" });
  } catch (error) {
    console.log(error);
  }
});

module.exports = router;
