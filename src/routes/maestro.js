const express = require("express");

const maestroSchema = require("../model/maestro");
const router = express.Router();

router.post("/maestros", async (req, res) => {
  // res.send("maestro creado")
  const maestro = await maestroSchema(req.body);
  maestro
    .save()
    .then((data) => {
      res.json(data);
    })
    .catch((err) => {
      res.json({ mensaje: err });
    });
});

router.get("/maestros", async (req, res) => {
  try {
    const maestro = await maestroSchema.find().exec();
    res.json(maestro);

    console.log(maestro);
  } catch (error) {
    console.log(error);
  }
});

router.put("/maestros/:id", async (req, res) => {
    const {id} = req.params;
    console.log(id);

  
  const maestro = await maestroSchema(req.body);
  console.log(maestro);
  maestroSchema.findByIdAndUpdate(
    id, {$set: {
        numempleado : maestro.numempleado,
        idusuario : maestro.idusuario,
        materia : maestro.materia,
        rfc : maestro.rfc
        }}
    , function (err, docs) {
    if (err){
        console.log(err)
        res.json({ mensaje: err });
    }
    else{
        console.log("actualizado : ", docs);
        res.json({ mensaje: "actualizado" });
    }

});
});

router.delete("/maestros/:id", async (req, res) => {
  try {
    const {id} = req.params;  
    const eliminado = await maestroSchema.findByIdAndDelete(id);
    res.json({ mensaje: "Eliminado" });
  } catch (error) {
    console.log(error);
  }
});

module.exports = router;
