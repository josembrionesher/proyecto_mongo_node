const express = require("express");

const materiaSchema = require("../model/materias");
const router = express.Router();

router.post("/materias", async (req, res) => {
  // res.send("materia creado")
  const materia = await materiaSchema(req.body);
  materia
    .save()
    .then((data) => {
      res.json(data);
    })
    .catch((err) => {
      res.json({ mensaje: err });
    });
});

router.get("/materias", async (req, res) => {
  try {
    const materia = await materiaSchema.find().exec();
    res.json(materia);

    console.log(materia);
  } catch (error) {
    console.log(error);
  }
});

router.put("/materias/:id", async (req, res) => {
    const {id} = req.params;
    console.log(id);

  
  const materia = await materiaSchema(req.body);
  console.log(materia);
  materiaSchema.findByIdAndUpdate(
    id, {$set: {
        nombre : materia.nombre,
        creditos : materia.creditos
        }}
    , function (err, docs) {
    if (err){
        console.log(err)
        res.json({ mensaje: err });
    }
    else{
        console.log("actualizado : ", docs);
        res.json({ mensaje: "actualizado" });
    }

});
});

router.delete("/materias/:id", async(req, res) => {
  try {
    const {id} = req.params;  
    const eliminado = await materiaSchema.findByIdAndDelete(id);
    res.json({ mensaje: "Eliminado" });
  } catch (error) {
    console.log(error);
  }
});

module.exports = router;
