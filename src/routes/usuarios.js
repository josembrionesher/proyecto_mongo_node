const express = require("express");

const usuarioSchema = require("../model/usuario");
const router = express.Router();

router.post("/usuarios", async (req, res) => {
  // res.send("usuario creado")
  const usuario = await usuarioSchema(req.body);
  usuario
    .save()
    .then((data) => {
      res.json(data);
    })
    .catch((err) => {
      res.json({ mensaje: err });
    });
});

router.get("/usuarios", async (req, res) => {
  try {
    const usuario = await usuarioSchema.find().exec();
    res.json(usuario);

    console.log(usuario);
  } catch (error) {
    console.log(error);
  }
});

router.put("/usuarios/:id", async (req, res) => {
    const {id} = req.params;
    console.log(id);

  
  const usuario = await usuarioSchema(req.body);
  console.log(usuario);
  usuarioSchema.findByIdAndUpdate(
    id, {$set: {
        nombre: usuario.nombre,
    apellido: usuario.apellido,
    edad :usuario.edad,
    curp: usuario.curp 
        }}
    , function (err, docs) {
    if (err){
        console.log(err)
        res.json({ mensaje: err });
    }
    else{
        console.log("actualizado : ", docs);
        res.json({ mensaje: "actualizado" });
    }

});
});

router.delete("/usuarios/:id", async (req, res) => {
    
    try {
      const {id} = req.params;  
      const eliminado = await usuarioSchema.findByIdAndDelete(id);
      res.json({ mensaje: "Eliminado" });
    } catch (error) {
      console.log(error);
    }
    
});


router.get("/usuarios/:id", async (req, res) => {
    
  try {
    const {id} = req.params;  
    const ver = await usuarioSchemta.findById(id);
    res.json(ver);
  } catch (error) {
    console.log(error);
  }
  
});


router.post("/agregar", async (req, res) => {
  
  const usuario = await (usuarioSchema(req.body));
  console.log(usuario);
    usuario
      .save()
      .then((data) => {
       

       res.json("subido")
       console.log(data);

       const valor =data.id
       busqueda(valor)
       
           
     })
     .catch((err) => {
       res.json({ mensaje: err });
     });
});


function busqueda(valor) {
  try {
    
    router.get(valor, async (req, res) => {
    
      try {
          
        const ver = await usuarioSchemta.findById(valor);
        (ver)=>{
          if (!ver) {
            console.log("no existen datos");
          } else {
            router.put(ver, async (req, res) => {
              
          
            
            const usuario = await usuarioSchema(req.body);
            
            console.log(usuario);
            usuarioSchema.findByIdAndUpdate(
              id, {$set: {
                  nombre: usuario.nombre,
              apellido: usuario.apellido,
              edad :usuario.edad,
              curp: usuario.curp 
                  }}
              , function (err, docs) {
              if (err){
                  console.log(err)
                  res.json({ mensaje: err });
              }
              else{
                  console.log("actualizado : ", docs);
                  res.json({ mensaje: "actualizado" });
              }
          
          });
          });
          
          }  
        }
        
      } catch (error) {
        console.log(error);
      }
      
    });    

    
  } catch (error) {
    console.log(error);
  }
}


module.exports = router;
