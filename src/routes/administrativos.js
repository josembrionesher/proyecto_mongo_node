const express = require("express");

const admSchema = require("../model/administrativos");
const router = express.Router();

router.post("/adm", async (req, res) => {
  // res.send("adm creado")
  const adm = await admSchema(req.body);
  adm
    .save()
    .then((data) => {
      res.json(data);
    })
    .catch((err) => {
      res.json({ mensaje: err });
    });
});

router.get("/adm", async (req, res) => {
  try {
    const adm = await admSchema.find().exec();
    res.json(adm);

    console.log(adm);
  } catch (error) {
    console.log(error);
  }
});

router.put("/adm/:id", async (req, res) => {
    const {id} = req.params;
    console.log(id);

  
  const adm = await admSchema(req.body);
  console.log(adm);
  admSchema.findByIdAndUpdate(
    id, {$set: {
        numtrabajador :adm.numtrabajador,
        idusuario : adm.idusuario,
        rfc : adm.rfc,
        }}
    , function (err, docs) {
    if (err){
        console.log(err)
        res.json({ mensaje: err });
    }
    else{
        console.log("actualizado : ", docs);
        res.json({ mensaje: "actualizado" });
    }

});
});

router.delete("/adm/:id", async (req, res) => {
  try {
    const {id} = req.params;  
    const eliminado = await admSchema.findByIdAndDelete(id);
    res.json({ mensaje: "Eliminado" });
  } catch (error) {
    console.log(error);
  }
});

module.exports = router;
