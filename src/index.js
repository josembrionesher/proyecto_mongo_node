const express = require('express')
const mongoose = require('mongoose');
const app = express()
require('dotenv').config()

const rutasUsuario = require('./routes/usuarios');
 const rutasAdm = require('./routes/administrativos');
 const rutasAreas = require('./routes/areaslimpieza');
 const rutasEst = require('./routes/estudiante');
 const rutasMae = require('./routes/maestro');
 const rutasMat = require('./routes/materias');

const port = process.env.PORT || 3000
app.use(express.json());

//middleware
app.use('/api', rutasUsuario);
app.use('/api', rutasAdm);
app.use('/api', rutasAreas);
app.use('/api', rutasEst);
app.use('/api', rutasMae);
app.use('/api', rutasMat);

const usuario = process.env.BD_USUARIO,
  password = process.env.DB_PASS,
  database =process.env.DB_DATABASE,
  cluster= process.env.DB_CLUSTER
 const url = `mongodb+srv://${usuario}:${password}@${cluster}.hmwx0t5.mongodb.net/${database}?retryWrites=true&w=majority`;


//routes

app.get('/' , (req , res)=>{
   res.send('hello from simple server :)')
})

mongoose
  .connect(url).then(() => {
    console.log("conectado");
  })
  .catch((e) => {
    console.log(e);
  });
app.listen(port , ()=> console.log('> Server is up and running on port : ' + port))